+++
judul = "Mencoba Citra"
penulis = ["Penulis 1","Penulis 2","Penulis 3"]
tanggal = "2019-12-30T14:45:59Z"
deskripsi = "Mencoba pengatakan **citra**; **grafis**, **gambar**, **foto**, **diagram**"
tal = [
    "graphic",
    "image",
    "photo",
]
dan = [
    "coba",
]
dokumen = [
    "test",
    "graphic",
    "grafis",
    "image",
    "citra",
    "photo",
    "foto",
]
+++

blog dua
## `gfx` test
### Penanda bawaan untuk citra
![Foto besar menggunakan penanda bawaan](kopi-720.jpg)

Foto kecil dalam baris ![Foto kecil menggunakan penanda bawaan](langit-256.jpg) seperti ini.

### Citra memakai *shortcodes* bawaan `figure`
{{< figure src="kopi-720.jpg" title="figure Image caption" >}}

## shortcodes `gfx-hero`
{{< gfx-hero "kopi-720.jpg" >}}

Ini mencoba sematan dengan lebar penuh.