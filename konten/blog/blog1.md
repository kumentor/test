+++
judul = "Mencoba Ikon / *Icon* SVG (*Scalable Graphic Vector*)"
penulis = ["Penulis 1","Penulis 2","Penulis 3","Penulis 4","Penulis 5","Penulis 6","Penulis 7","Penulis 8","Penulis 9","Penulis 10","Penulis 11","Penulis 12","Penulis 13","Penulis 14","Penulis 15","Penulis 16","Penulis 17","Penulis 18"]
tanggal = "2019-12-30T14:45:59Z"
deskripsi = "Deskripsi blog satu."
tal = [
    "graphic",
    "icon",
    "svg",
]
dan = [
    "coba",
]
dokumen = [
    "test",
    "grafis",
]
twitter = "tw_username"
facebook = "fb_username"
telegram = "tg_username"
instagram = "ig_username"
youtube = "yt_username"
disqus = "disqus"
tweet = "https://twitter.com/Twitter/status/1143624280401502208"
fbpost = "https://www.facebook.com/20531316728/posts/10154009990506729"
tgpost = "t.me/durov/43"
igpost = "https://instagram.com/p/B8HQ7b-Ak1q"
ytvideo = "https://www.youtube.com/watch?v=lJIrF4YjHfQ"
+++

blog satu

## `ikon` test
### ux icon
+ {{< ikon kumentor >}} kumentor  
+ {{< ikon atak >}} atak  
+ {{< ikon aksesibilitas >}} aksesibilitas  
+ {{< ikon bagi >}} bagi  
+ {{< ikon kirim >}} kirim  
+ {{< ikon susur >}} susur  
+ {{< ikon terang >}} terang  
+ {{< ikon gelap >}} gelap 
+ {{< ikon folder >}} folder  
+ {{< ikon folder-o >}} folder-o 
+ {{< ikon avatar >}} avatar  
+ {{< ikon avatar-o >}} avatar-o  
+ {{< ikon bahasa >}} bahasa  

### social icon
+ {{< ikon disqus >}} disqus  
+ {{< ikon facebook >}} facebook  
+ {{< ikon github >}} github  
+ {{< ikon google >}} google  
+ {{< ikon instagram >}} instagram  
+ {{< ikon line >}} line  
+ {{< ikon mastodon >}} mastodon  
+ {{< ikon telegram >}} telegram  
+ {{< ikon twitter >}} twitter  
+ {{< ikon whatsapp >}} whatsapp  
+ {{< ikon youtube >}} youtube  
+ {{< ikon gitlab >}} gitlab  
+ {{< ikon hugo >}} hugo 