---
title: "Mencoba Tabel"
penulis: Documentator2
deskripsi: "Ini adalah deskripsi `yaml`"
dan:
- sosial
- coba
- coba/test
sekolah:
- madya
- madya/pertama
buku:
- sekolah
- sosial
---

doc dua

Name    | Age
--------|------
Bob     | 27
Alice   | 23
--------|------
Name    | Age

name|x|y|unit|group|class|dur|update
---|---|---|---|---|---|---|---
HD720|1280|720|px|16:9|video||
HD720 crop 9:16 portrait|405|720|px|9:16|video||
CGA|320|200|px|16:10|video||
QVGA|320|240|px|4:3 |video||
SIF|384|288|px||video||
VGA (NTSC)|640|480|px|4:3|video||
PAL (SECAM)|768|576|px|4:3|video||
SVGA|800|600|px|4:3|video||
XGA|1024|768|px|4:3|video||
XGA+|1152|864|px|4:3|video||
|1280|854|px||video||
|1281|960|px||video||
SXGA+|1400|1050|px|4:3|video||
|1440|1080|px||video||
UXGA|1600|1200|px|4:3|video||
QXGA|2048|1536|px|4:3|video||
QSXGA|2560|2048|px|4:3|video||
CIF|352|288|px||video||
HVGA|480|320|px|3:2|video||
WVGA (NTFS)|854|480|px|16:9|video||
PAL (SECAM)|1024|576|px|16:9|video||
WSVGA|1024|600|px||video||
|1152|768|px||video||
WXGA|1280|768|px||video||
WXGA|1280|800|px|16:10|video||
|1440|900|px||video||
|1600|900|px||video||
|1440|960|px||video||
WSXGA+|1680|1050|px|16:10|video||
HD1080|1920|1080|px|16:9|video||
HD1080 crop 9:16 portrait|607,5|1080|px|9:16|video||
HD1080 crop 4:5 portrait|864|1080|px|4:5|video||
2K|2048|1080|px|17:9|video||
WUXGA|1920|1200|px|16:10|video||
UWUXGA|2560|1080|px||video||
WQHD|2560|1440|px||video||
WQXGA|2560|1600|px||video||
UHD-1|3840|2160|px||video||
4K|4096|2160|px||video||

Name    | Age
--------|------
Bob     | 27
Alice   | 23

name|x|y|unit|group|class|x2|y2|unit2
---|---|---|---|---|---|---|---|---
2R|2,5|3,5|inch|r|photo|||mm
3R|3,5|5|inch|r|photo|88,9|127|mm
4R|4|6|inch|r|photo|101,6|152,4|mm
5R|5|7|inch|r|photo|127|177,8|mm
6R|6|8|inch|r|photo|||mm
8R, 6P|8|10|inch|r|photo|203,2|254|mm
8Rs|8|12|inch|r|photo|203,2|304,8|mm
10R|10|12|inch|r|photo|254|304,8|mm
10Rs|10|15|inch|r|photo|254|304,8|mm
11R|11|14|inch|r|photo|279|356|mm
11Rs|11|17|inch|r|photo|279|432|mm
12R|12|15,5|inch|r|photo|304,8|393,7|mm
12Rs|12|18|inch|r|photo|304,8|465|mm
16R|16|20|inch|r|photo|406,4|508|mm
20R|20|24|inch|r|photo|508|609,6|mm
24R|24|31,5|inch|r|photo|609,6|800|mm
30R|30|40|inch|r|photo|750|1000|mm
Spectra|3,62|2,87|inch||photo|||mm
Spectra frame|4|4,1|inch||photo|||mm
SX-70|3,1|3,1|inch||photo|||mm
SX-70 frame|3,5|4,2|inch||photo|||mm
Type100|2,84|3,74|inch||photo|||mm
Type100 frame|3,25|4,25|inch||photo|||mm
Pass 2x3|||inch||photo|20|30|mm
Pass 3x4|||inch||photo|30|40|mm
Pass 4x6|||inch||photo|40|60|mm
Polaroid|3,1|3,1|inch||photo|||mm
Polaroid frame|3,5|4,2|inch||photo|||mm
4D|4,5|6|inch||photo|||mm
ANSI A, Letter|8,5|11|inch|ansi|paper|||mm
ANSI B, Ledger, Tabloid|11|17|inch|ansi|paper|||mm
ANSI C|17|22|inch|ansi|paper|||mm
ANSI D|22|34|inch|ansi|paper|||mm
ANSI E|34|44|inch|ansi|paper|||mm
4A0|||inch|a|paper|1682|2378|mm
2A0|||inch|a|paper|1189|1682|mm
A0|||inch|a|paper|841|1189|mm
A1|||inch|a|paper|594|841|mm
A2|||inch|a|paper|420|594|mm
A3|||inch|a|paper|297|420|mm
A4|||inch|a|paper|210|297|mm
A5|||inch|a|paper|148|210|mm
A6|||inch|a|paper|105|148|mm
A7|||inch|a|paper|74|105|mm
A8, ISO 216|||inch|a|paper|52|74|mm
B0|||inch|b|paper|1000|1414|mm
B1|||inch|b|paper|707|1000|mm
B2|||inch|b|paper|500|707|mm
B3|||inch|b|paper|353|500|mm
B4|||inch|b|paper|250|353|mm
B5|||inch|b|paper|176|250|mm
B6|||inch|b|paper|125|176|mm
B7|||inch|b|paper|88|125|mm
B8|||inch|b|paper|62|88|mm
C0|||inch|c|paper|917|1297|mm
C1|||inch|c|paper|648|917|mm
C2|||inch|c|paper|458|648|mm
C3|||inch|c|paper|324|458|mm
C4|||inch|c|paper|229|324|mm
C5|||inch|c|paper|162|229|mm
C6|||inch|c|paper|114|162|mm
C7|||inch|c|paper|81|114|mm
C8|||inch|c|paper|57|81|mm
Australian/NZ broadsheet|||inch|newspaper|paper|420|594|mm
Canadian tabloid|||inch|newspaper|paper|260|368|mm
Canadian tall tab|||inch|newspaper|paper|260|413|mm
Norwegian tabloid|||inch|newspaper|paper|280|400|mm
British tabloid/compact|||inch|newspaper|paper|280|430|mm
Berliner/midi|||inch|newspaper|paper|315|470|mm
Swiss/Neue Zërcher Zeitung|||inch|newspaper|paper|320|475|mm
Ciner Format|||inch|newspaper|paper|350|500|mm
Rhenish|||inch|newspaper|paper|350|520|mm
Nordisch|||inch|newspaper|paper|400|570|mm
US broadsheet|||inch|newspaper|paper|381|578|mm
New York Times|||inch|newspaper|paper|305|559|mm
Wall Street Journal|||inch|newspaper|paper|305|578|mm
South African broadsheet|||inch|newspaper|paper|410|578|mm
British broadsheet|||inch|newspaper|paper|375|597|mm
Bussines card, ISO 216|||inch|business|card|74|52|mm
US Bleed size|3,75|2,25|inch|business|card|||mm
US Standard|3,5|2|inch|business|card|||mm
UK Bleed size|||inch|business|card|91|61|mm
UK Standard|||inch|business|card|85|55|mm
Banking card|3,37|2,125|inch|business|card|||mm
Business card||||||||
in Western Europe|||inch|business|card|85|55|mm
Japan Standard|||inch|business|card|91|55|mm
Hong Kong, China,||||||||
Singapore, Malaysia|||inch|business|card|90|54|mm
STMT|8,5|5,5|inch|us|paper|||mm
Elephant|32|28|inch|us|paper|||mm
Medium|18|23|inch|us|paper|||mm
Royal|20|25|inch|us|paper|||mm
Quad demy|35|45|inch|us|paper|||mm
Double demy|23,5|35|inch|us|paper|||mm
Post|15,5|19,25|inch|us|paper|||mm
Crown|15|20|inch|us|paper|||mm
Lanrge post|16,5|21|inch|us|paper|||mm
Demy|17,5|22,5|inch|us|paper|||mm
Quarto|8|10|inch|us|paper|||mm
Executive|7,25|10,5|inch|us|paper|||mm
Government letter|8,5|10,5|inch|us|paper|||mm
Letter|8,5|11|inch|us|paper|||mm
Foolscap folio|8,5|13,5|inch|us|paper|||mm
Legal|8,5|14|inch|us|paper|||mm
Ledger, Tabloid|11|17|inch|us|paper|||mm
PA4|8,3|11|inch|||||mm
Arch A|9|12|inch|||||mm